---
name: Daily Harvest
tier: startup
site_url: https://www.daily-harvest.com/careers
logo: daily-harvest.png
---
Founded in 2015, Daily Harvest offers a solution to the challenge of eating
nutrient-rich, unprocessed meals while leading a busy life. The New York-based
direct-to-consumer company delivers convenient, plant-based smoothies,
overnight oats, chia parfaits, soups, Harvest Bowls, and functional lattes to
freezers across the US. As one of the fastest growing e-commerce brands, Daily
Harvest is reimagining the frozen food category using a “farm-freezing” process
— harvesting fruits and vegetables at peak ripeness and immediately freezing
them to lock in nutrients and flavor. Each recipe combines organic produce with
nutrient-dense superfoods and is ready to eat in minutes, prepared by adding a
liquid and blending, soaking, or heating. For more information, visit
www.daily-harvest.com.
