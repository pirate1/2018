---
abstract: What do Superman III, Hackers, and Office Space all have in common? Find
  out in this talk, along with some concurrency, database integrity, and financial
  data safety fundamentals. Hear tales of what we did wrong and right in the process
  of building a real-time Poker platform with django-channels.
duration: 30
level: Intermediate
room: Madison
slot: 2018-10-06 10:15:00-04:00
speakers:
- Nick Sweeting
title: 'How I Learned to Stop Worrying and Love atomic(): Banking Blunders and Concurrency
  Challenges'
type: talk
presentation_url: https://pirate.github.io/django-concurrency-talk/
---

This is half overview of distributed systems fundamentals, half tales of our adventures at OddSlingers designing a django-channels backend to power an online poker platform.  We'll cover some common financial data pitfalls, get into some more advanced nuances of transactions, and discuss what modern "next generation distributed  SQL" databases like CockroachDB and TiDB do, and how it all ties into to building a globally distributed apps.  We'll also cover the benefits of log-structured data and how it makes reasoning about stateful systems easier.

Did you know every Django app is already a distributed system, even when it's running only on one server? Have you heard of .select_for_update() and transaction.atomic()? Did you know that even when using them, there are at least 3 different ways you can encounter DB consistency bugs?  Come learn about all the ways it's possible to screw up when handling sensitive data, and how to avoid them!