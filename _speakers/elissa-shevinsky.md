---
name: Elissa Shevinsky
talks:
- 'Secret Histories: The Women of Python'
---

Elissa Shevinsky is a serial entrepeneur. She helped launch Geekcorps (acquired), Everyday Health (IPO), Daily Steals and Brave ($35M IPO.) She is also the Editor of "Lean Out: The Struggle for Gender Equality in Tech and Start-up Culture." "Lean Out" was included in the "Top 100 Business Books of 2015" by Inc and "Big Indie Books of 2015" by Publishers Weekly.