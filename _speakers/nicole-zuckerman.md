---
name: Nicole Zuckerman
talks:
- Best Intentions, Least Measured
---

Nicole attended Hackbright Academy in 2012, and has been in love with python ever since. She can currently be found at Clover Health, tackling with glee data pipelines and APIs and everything in between. In her free time, Nicole is an avid dancer and teacher, sci-fi book fanatic, soul and jazz aficionado, and cheese lover. She has an MA in English Literature and Women’s Studies from the University of Liverpool.

Sara is a software engineer at Clover Health where she builds web applications to help make healthcare work, but she wasn't always! Before joining Clover Health she attended Hackbright and previously worked in a field completely unrelated to tech. When not writing software or learning about how to write software, she usually reading, cooking, hiking, skiing, or dancing.